import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';


@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.css']
})
export class ShowEventComponent implements OnInit {

  eventDetail;
  constructor(private loginService: LoginService) { 
    this.loginService.getEvents().subscribe((response :any)=>{
      console.log(response);
      this.eventDetail = response.data;
    });
  }

  ngOnInit() {
    
  }

  
}

import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import{ RegisterService } from '../services/register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder,private rs: RegisterService) {
   }

 /*  addUser(firstname,lastname,email,password) {
    this.rs.addUser(firstname,lastname,email,password);
  } */

  ngOnInit() {
    
      this.registerForm = this.fb.group({
        firstname: ['',[Validators.required,Validators.pattern('^[a-zA-Z]+$')]],
        lastname: ['',Validators.required],
        email: ['',[Validators.required,Validators.email]],
        password:['',[Validators.required,Validators.minLength(5)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit(){
    this.submitted = true;
    if(this.registerForm.invalid){
      console.log('form is invalid');
      return;
    }
    else 
    { 
      console.log(this.registerForm.value);
      this.rs.registerData(this.registerForm.value);
    }
  }

}

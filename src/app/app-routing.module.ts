import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent} from './dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AddEventComponent } from './add-event/add-event.component';
import { ShowEventComponent } from './show-event/show-event.component';

const routes: Routes = [
  {path: '',pathMatch:'full',redirectTo:'login'},
  { path: 'login',component:LoginComponent },
  { path: 'register',component:RegisterComponent},
  { path: 'dashboard',component:DashboardComponent,canActivate:[AuthGuardService]},
  { path: 'add-event',component:AddEventComponent,canActivate:[AuthGuardService]},
  {path: 'show-event',component:ShowEventComponent,canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginService } from './services/login.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterService } from './services/register.service';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AddEventComponent } from './add-event/add-event.component';
import { ShowEventComponent } from './show-event/show-event.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SocialLoginModule, AuthServiceConfig,GoogleLoginProvider,FacebookLoginProvider,LinkedInLoginProvider} from 'angularx-social-login';

// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("2305203666422910")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("509622849997-798pupr560qokt9qau0kh40lg6c5ckml.apps.googleusercontent.com")
        },
          {
            id: LinkedInLoginProvider.PROVIDER_ID,
            provider: new LinkedInLoginProvider("81iz2fk1dn3pyh",false,'en_US')
          },
      ]
  )
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    AddEventComponent,
    ShowEventComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    SocialLoginModule
  ],
  providers: [{provide:AuthServiceConfig,useFactory:getAuthServiceConfigs},LoginService,RegisterService,AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }

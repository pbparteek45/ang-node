import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { Router,CanActivate} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

constructor(protected router: Router,protected loginService: LoginService) { }

  canActivate(){
    if(this.loginService.isLoggedIn()) return true;

    this.router.navigate(['/login']);

    return false;
  }

}


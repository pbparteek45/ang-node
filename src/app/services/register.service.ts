import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  // url = 'http://localhost:3001/register';

  constructor(private http: HttpClient) { }

  registerData(data){
    this.http.post("http://localhost:3001/register",data).subscribe(res=>console.log('Done'));
  }

  
}

import { Injectable } from '@angular/core';
import { HttpRequest,HttpInterceptor,HttpClient,HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user;
  httpOptions;
  private url = "http://localhost:3001";

  constructor(private http: HttpClient) {
    this.httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': localStorage.getItem('token'),
    })
    }
   }

  loginData(data){
    return this.http.post(this.url+'/signin',data)
    .pipe(map(response=> {
      this.user = response;
      console.log(this.user);
      if(this.user && this.user.token){
        localStorage.setItem('token',this.user.token)
        return true;
      }
     else {
       return false;
     }
    }))

  }

  isLoggedIn(){
    let token = localStorage.getItem('token');
    if(token){
      return true;
    }
    else {
      return false;
    }
  }

  logout() { 
    localStorage.removeItem('token');
    this.user = null;
  }

  getEvents(){
   return this.http.get(this.url+'/show_events',this.httpOptions)
   .pipe(map(response=>{
     let eventDetail = response;
     return eventDetail;
     console.log(eventDetail);
   }))
  }

  createEvent(data){
    return this.http.post(this.url+'/add_events',data,this.httpOptions)
    .pipe(map(response=>{
      this.user = response;
    return response;
    }))
  }
}

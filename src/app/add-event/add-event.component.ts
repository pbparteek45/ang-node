import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  eventForm: FormGroup;
  submitted: boolean;
  invalidLogin: boolean;
  constructor(private fb: FormBuilder,private lg: LoginService) { }

  ngOnInit() {
    this.eventForm = this.fb.group({
      eventname: ['',Validators.required],
      event_date: ['',Validators.required],
      event_type: ['',Validators.required],
      event_subcategory:['',Validators.required]
  });
  }

  onSubmit(){
    this.submitted = true;
    if(this.eventForm.invalid){
      console.log('form is invalid');
      return;
    }
    else 
    { 
      console.log(this.eventForm.value);
      this.lg.createEvent(this.eventForm.value).subscribe(response=>{
        if(response){
          console.log(response);
        }
        else {
          this.invalidLogin==true;
        }
      });
    }

  }
}

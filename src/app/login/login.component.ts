import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { User } from '../models';
import { Router } from '@angular/router';
import { AuthService,FacebookLoginProvider,GoogleLoginProvider,LinkedInLoginProvider,SocialUser} from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalidLogin: boolean;
  loginForm: FormGroup;
  private user: SocialUser;
  constructor(private fb: FormBuilder,private router: Router,private ls: LoginService,private socialAuthService: AuthService) {
  }

   

  ngOnInit() {
      this.loginForm = this.fb.group({
        email: ['', [Validators.required, Validators.pattern(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/)]],
        password: ['', [Validators.required]]
      });
      /* this.socialAuthService.authState.subscribe((user) => {
        this.user = user;
      }); */
  }

  /* signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
 
  signInWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signInWithLinkedIn():void{
    this.socialAuthService.signIn(LinkedInLoginProvider.PROVIDER_ID);
  } */

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if(socialPlatform == "Linkedin"){
      socialPlatformProvider = LinkedInLoginProvider.PROVIDER_ID;
    } 
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        
      }
    );
  }

  signOut(): void {
    this.socialAuthService.signOut();
  }
  
  login(){
    if(this.loginForm.valid){
      alert('form is valid');
      console.log(this.loginForm.value);
      this.ls.loginData(this.loginForm.value).subscribe(user=>{
        if(user)
          this.router.navigate(['/dashboard']);
          else
          this.invalidLogin = true;
      });

    }
    else{
      alert('form is not valid');
    }
  }

}
